﻿using System;
using NoteProcesser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting {
	[TestClass]
	public class MeausreTests {
		[TestMethod]
		[ExpectedException ( typeof ( ArgumentOutOfRangeException ) )]
		public void TestMeasureIndexingError_1 ( ) {
			Measure m = new Measure ( );
			Note e = m [ 0 ];
		}
		[TestMethod]
		[ExpectedException ( typeof ( IndexOutOfRangeException ) )]
		public void TestMeasureIndexingError_2 ( ) {
			Measure m = new Measure ( );
			Note e = m [ "something" ];
		}

		[TestMethod]
		public void TestMeasureConstructor ( ) {
			Note n = new Note ( "drum|quarter" );
			Measure m = new Measure ( n );
			Assert.AreEqual ( n , m [ 0 ] );
		}

		[TestMethod]
		public void TestMeasureIndexing ( ) {
			Measure m = new Measure ( );
			Note n1 = new Note ( "snare|whole" );
			Note n2 = new Note ( "bass|half" );
			Note n3 = new Note ( "drum|quarter" );
			Note n4 = new Note ( "cynbal|eighth" );

			m.AddNote ( n1 );
			m.AddNote ( n2 );
			m.AddNote ( n3 );
			m.AddNote ( n4 );

			Assert.AreEqual ( 4 , m.size );

			Assert.AreEqual ( n1 , m [ "first" ] );
			Assert.AreEqual ( n4 , m [ "last" ] );

			Assert.AreEqual ( n1 , m [ 0 ] );
			Assert.AreEqual ( n2 , m [ 1 ] );
			Assert.AreEqual ( n3 , m [ 2 ] );
			Assert.AreEqual ( n4 , m [ 3 ] );
		}
	}
}
