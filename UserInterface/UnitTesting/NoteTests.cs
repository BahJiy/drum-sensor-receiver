﻿using System;
using NoteProcesser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting {
	[TestClass]
	public class NoteTests {
		[TestMethod]
		public void TestNoteFields ( ) {
			Note.PopulateData ( );
			Note n = new Note ( "snare|quarter" );
			Note n2 = new Note ( "drum | half" );
			Note n3 = new Note ( "bass|  eighth" );

			Assert.AreEqual ( "snare" , n.noteName );
			Assert.AreEqual ( 0.25 , n.noteValue );
			Assert.IsNull ( n.xLocation );
			Assert.IsNull ( n.yLocation );

			Assert.AreEqual ( "drum" , n2.noteName );
			Assert.AreEqual ( 0.5 , n2.noteValue );
			Assert.IsNull ( n2.xLocation );
			Assert.IsNull ( n2.yLocation );

			Assert.AreEqual ( "bass" , n3.noteName );
			Assert.AreEqual ( 0.125 , n3.noteValue );
			Assert.IsNull ( n3.xLocation );
			Assert.IsNull ( n3.yLocation );
		}

		[TestMethod]
		public void TestNoteIndexing ( ) {
			Note.PopulateData ( );
			Note n = new Note ( "snare|quarter" );
			Note n2 = new Note ( "drum | half" );
			Note n3 = new Note ( "bass|  eighth" );

			Assert.AreEqual ( "snare" , n [ 0 ] );
			Assert.AreEqual ( "0.25" , n [ 1 ] );
			Assert.IsNull ( n [ 2 ] );
			Assert.IsNull ( n [ 3 ] );

			Assert.AreEqual ( "drum" , n2 [ 0 ] );
			Assert.AreEqual ( "0.5" , n2 [ 1 ] );
			Assert.IsNull ( n2 [ 2 ] );
			Assert.IsNull ( n2 [ 3 ] );

			Assert.AreEqual ( "bass" , n3 [ 0 ] );
			Assert.AreEqual ( "0.125" , n3 [ 1 ] );
			Assert.IsNull ( n3 [ 2 ] );
			Assert.IsNull ( n3 [ 3 ] );
		}

		[TestMethod]
		[ExpectedException(typeof(IndexOutOfRangeException))]
		public void TestNoteIndexingException ( ) {
			Note.PopulateData ( );
			Note n = new Note ( "snare|quarter" );
			string x = n [ 4 ];
		}
	}
}
