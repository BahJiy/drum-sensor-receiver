﻿using System;
using System.IO.Ports;
using ComPortReader;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting {
	[TestClass]
	public class ComReaderTests {
		[TestMethod]
		public void TestDataPackage ( ) {
			DataPackage dp = new DataPackage ( new byte [ 5 ] { 0 , 1 , 2 , 3 , 4 , } );
			Assert.AreEqual ( 5 , dp.size );
			for ( int i = 0; i < 5; i++ ) {
				Assert.AreEqual ( i , dp [ i ] );
			}
		}

		[TestMethod]
		[ExpectedException ( typeof ( Exception ) )]
		public void TestComReaderError ( ) {
			ComReader cr = new ComReader ( "NOCOM" );
		}

		[TestMethod]
		public void TestComReaderConstructor ( ) {
			if ( SerialPort.GetPortNames ( ).Length == 0 ) {
				ComReader cr = new ComReader ( delegate { return; } );
				Assert.IsFalse ( cr.FindArduino ( ) );
			} else
				Assert.Inconclusive ( "There are avialbable open" );
		}
	}
}
