﻿using System;
using System.Collections.Generic;
using NoteProcesser;
using ComPortReader;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTesting {
	[TestClass]
	public class ProcesserTests {
		[TestMethod]
		public void TestProcessDataEvent ( ) {
			Processer p = new Processer ( delegate {
				return;
			} );

			DataPackage dp = new DataPackage ( new byte [ 10 ] {
			0 , 0, 8, 1, 4, 2, 6, 3, 0, 0 } );

			p.processedNote += delegate (object sender, List<Note> e) {
				Assert.AreEqual ( 4 , e.Count );
				Assert.AreEqual ( new Note ( "note 0|whole" ) , e [ 0 ] );
				Assert.AreEqual ( new Note ( "note 8|half" ) , e [ 0 ] );
				Assert.AreEqual ( new Note ( "note 4|quarter" ) , e [ 0 ] );
				Assert.AreEqual ( new Note ( "note 6|eighth" ) , e [ 0 ] );
			};
		}
	}
}
