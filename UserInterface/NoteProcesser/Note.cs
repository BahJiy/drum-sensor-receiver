﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace NoteProcesser {
	public class Note {
		public string this [ int i ] {
			get {
				switch ( i ) {
					case 0:
						return noteName;
					case 1:
						return noteValue.ToString ( );
					case 2:
						return xLocation?.ToString ( );
					case 3:
						return yLocation?.ToString ( );
					default:
						throw new IndexOutOfRangeException ( "Value only valid between 0 - 2" );
				}
			}
		}

		#region variables
		public readonly string noteName;
		public readonly double noteValue;

		public double? xLocation, yLocation;

		private static Dictionary<string , double> values { get; set; }
		#endregion variables

		/// <summary>
		/// Get a lsit of all image files for the notes and populate a the value of note allowed
		/// </summary>
		internal static void PopulateData ( ) => values = new Dictionary<string , double> ( ) {
			{"whole", 1.0 }, {"half", 0.5 }, {"quarter", 0.25 }, {"eighth", 0.125 } };

		/// <summary>
		/// Create a note with its data populated
		/// </summary>
		/// <param name="noteData">String pattern should be "note name | note type"</param>
		internal Note ( string noteData ) {
			string [ ] temp = noteData.Split ( '|' );
			noteName = temp [ 0 ].Trim ( );
			values.TryGetValue ( temp [ 1 ].Trim ( ) , out noteValue );
		}
	}
}
