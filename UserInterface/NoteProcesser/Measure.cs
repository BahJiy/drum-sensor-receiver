﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteProcesser {
	public class Measure {
		public Note this [ int i ] => noteList [ i ];
		public Note this [ string i ] {
			get {
				switch ( i.ToLower ( ) ) {
					case "first":
						return this [ 0 ];
					case "last":
						return this [ size - 1 ];
					default:
						throw new IndexOutOfRangeException ( "Only First and Last are valid inputs" );
				}
			}
		}

		#region variables
		public int size => noteList.Count;
		private List<Note> noteList;
		#endregion variables

		/// <summary>
		/// Create a measure
		/// </summary>
		/// <param name="x">The x lcoation</param>
		/// <param name="y">he y lcoation</param>
		/// <param name="note">If there needs to be a noted add also</param>
		public Measure ( Note note = null ) {
			noteList = new List<Note> ( );
			AddNote ( note );
		}

		/// <summary>
		/// Add a note to the measure. This will record and save the note data and information
		/// </summary>
		/// <param name="note"></param>
		public void AddNote ( Note note ) {
			if ( note != null )
				noteList.Add ( note );
		}
	}
}
