﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using ComPortReader;

namespace NoteProcesser {
	public delegate void ProcessedNoteEvent ( object sender , List<Note> e );

	public class Processer {
		public event ProcessedNoteEvent processedNote;

		public ComReader port { get; private set; }
		public bool connected => port.FindArduino ( );

		public Processer ( System.IO.Ports.SerialErrorReceivedEventHandler portErrorEvent ) {
			port = new ComReader ( portErrorEvent );
			port.dataReceived += ProcessData;
			Note.PopulateData ( );
		}

		/// <summary>
		/// Process the data received
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ProcessData ( object sender , DataPackage e ) {
			List<Note> noteList = new List<Note> ( );

			for ( int i = 0; i < e.size - 2; i++ ) {
				string noteText = "";
				switch ( e [ i ] ) {
					case 0:
						noteText += "note 0|";
						break;
					case 1:
						noteText += "note 1|";
						break;
					case 2:
						noteText += "note 2|";
						break;
					case 3:
						noteText += "note 3|";
						break;
					case 4:
						noteText += "note 4|";
						break;
					case 5:
						noteText += "note 5|";
						break;
					case 6:
						noteText += "note 6|";
						break;
					case 7:
						noteText += "note 7|";
						break;
					case 8:
						noteText += "note 8|";
						break;
					default:
						throw new ArgumentException ( "Invalid Bit detected: " + e[i] );
				}
				noteText += GetNoteValue ( e [ i + 2 ] );
				noteList.Add ( new Note ( noteText ) );
			}

			processedNote?.Invoke ( this , noteList );
		}

		/// <summary>
		/// Read the second bit and determine the note value
		/// </summary>
		/// <param name="b">Bit containing data for the note value. Should 
		/// be in the range of 0 - 3</param>
		/// <returns>Return the string text of the note value. Else throws an error</returns>
		internal string GetNoteValue ( byte b ) {
			switch ( b ) {
				case 0:
					return "whole";
				case 1:
					return "half";
				case 2:
					return "quarter";
				case 3:
					return "eighth";
				default:
					throw new ArgumentException ( "Invalid Bit detected: " + b );
			}
		}

		public void Dispose ( )=> port.Dispose ( );
	}
}