﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using NoteProcesser;

namespace UserInterface {

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow :IDisposable {
		public static List<Image> pictureList;

		private Processer processer;
		private List<Measure> measureList;
		private Thread reconnectThread;
		private Timer noteLineTimer;
		private Image noteLineImage;
		private Vector noteLinePos;

		public bool connected;
		public bool recording;

		public MainWindow ( ) {
			InitializeComponent ( );

			Closing += DisposeElement;
			connected = true;
			recording = false;

			//InitializePictures ( );

			measureList = new List<Measure> ( ) { new Measure ( ) };
			processer = new Processer ( new System.IO.Ports.SerialErrorReceivedEventHandler ( ErrorReceived ) );
			processer.processedNote += DisplayProcessNote;

			//determine if the Arduion is connected
			if ( processer.connected )
				LStatusLine.Content = "Connected to Arduino";
			else { // else we want to run a background task that tries to connect
				connected = false;
				reconnectThread = new Thread ( Reconnect ) { IsBackground = true };
				reconnectThread.Start ( );
			}

			noteLineTimer = new Timer ( new TimerCallback ( MoveNoteLine ) , null , Timeout.Infinite , 500 );
			noteLineImage = (Image)pictureList [ 0 ];
			noteLineImage.IsEnabled = false;
			noteLinePos = new Vector ( 10 , 155 );
			canvas.Children.Add ( noteLineImage );
		}

		/// <summary>
		/// Load all the images of the noteLine and Notes
		/// </summary>
		//public void InitializePictures ( ) => pictureList = new List<BitmapImage> ( ) {
		public void InitializePictures ( ) => pictureList = new List<Image> ( ) {
			//new BitmapImage(new Uri("images/noteLine.png",UriKind.Relative)),
			//new BitmapImage(new Uri("images/quarter.png", UriKind.Relative))};
			new Image() { Source = new BitmapImage(new Uri("images/noteLine.png",UriKind.Relative)), RenderSize = new Size(6, 65), Name = "noteLine"} ,
			new Image() { Source = new BitmapImage(new Uri("images/quarter.png", UriKind.Relative)), RenderSize = new Size(12, 30), Name = "quarter"} };

		/// <summary>
		/// Get the noted processed and dispaly it onscreen and store it
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DisplayProcessNote ( object sender , List<Note> e ) {
			double x = noteLinePos.X, y = noteLinePos.Y;
			int index = measureList.Count - 1;

			for ( int i = 0; i < e.Count; i++ ) {
				measureList [ index ].AddNote ( e [ i ] );
				// TODO
				// Display the note
				// Note size Hieght = 30 Width = 12

				// Find image picture and add it to the screen
				Image note = ( Image ) pictureList.Select ( z => z.Name.Equals ( e [ i ].noteName ) );
				//Image note = new Image ( ) { Source = ( ImageSource ) pictureList.Select ( z => z.UriSource.ToString ( ).Contains ( e [ i ].noteName ) ) };
				//Image note = new Image ( ) { Source = new BitmapImage ( new Uri ( "images/noteLine.png" , UriKind.Relative ))};

				Canvas.SetTop ( note , y );
				Canvas.SetLeft ( note , x );
				canvas.Children.Add ( note );
			}
		}

		/// <summary>
		/// This is run in the background to try to reconnect to the Arduino
		/// when needed
		/// </summary>
		private void Reconnect ( ) {
			string dots = "";
			int count = 0;
			while ( !processer.connected ) {
				switch ( count % 3 ) {
					case 0:
						dots = ".";
						break;
					case 1:
						dots = "..";
						break;
					case 2:
						dots = "...";
						break;
				}
				count++;
				Dispatcher.Invoke ( ( ) => LStatusLine.Content = "Error Connecting to Arduino - Retrying" + dots );
				Thread.Sleep ( 250 );
			}

			Dispatcher.Invoke ( ( ) => LStatusLine.Content = "Connected to Ardunio" );
			connected = true;
		}

		/// <summary>
		/// Move the Note Line based on a timer
		/// </summary>
		/// <param name="sender"></param>
		private void MoveNoteLine ( object sender ) {
			// space between upper and lower measure = ~~126
			// last meausre on right is ~~967
			// last measure on bottom is ~~837
			if ( noteLinePos.X > 970 ) {
				if ( noteLinePos.Y == 790 ) {
					noteLinePos.Y = 155;
					measureList.Add ( new Measure ( ) );
				} else
					noteLinePos.Y += 127;
				noteLinePos.X = 10;
			}

			Dispatcher.Invoke ( ( ) => {
				noteLineImage.IsEnabled = true;
				Canvas.SetLeft ( noteLineImage , noteLinePos.X += 3 );
				Canvas.SetTop ( noteLineImage , noteLinePos.Y );
			} );
		}

		/// <summary>
		/// Start/Stop recording
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void StartStop ( object sender , RoutedEventArgs e ) {
			if ( !recording ) {
				recording = true;
				BStart.Content = "Stop Recording";
				noteLineTimer.Change ( 10 , 10 );
			} else {
				recording = false;
				BStart.Content = "Start Recording";
				noteLineTimer.Change ( Timeout.Infinite , 500 );
			}
		}

		/// <summary>
		/// If we received an error in the Arduion connection, try reconnecting
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ErrorReceived ( object sender , System.IO.Ports.SerialErrorReceivedEventArgs e ) {
			if ( connected ) {
				reconnectThread = new Thread ( Reconnect ) { IsBackground = true };
				reconnectThread.Start ( );
			}
		}

		/// <summary>
		/// Dispose and clean up with the window is closing
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DisposeElement ( object sender , System.ComponentModel.CancelEventArgs e ) => Dispose ( );

		public void Dispose ( ) {
			Dispose ( true );
			GC.SuppressFinalize ( this );
		}
		
		protected virtual void Dispose(bool dispose) {
			if ( dispose ) {
				reconnectThread.Abort ( );
				noteLineTimer.Dispose();
				processer.Dispose ( );
				
			}
		}
		#region DEBUGGING
		int click = 0;
		private void DebugMouseMove ( object sender , MouseEventArgs e ) => LDebugMousePos.Content = e.GetPosition ( window );
		private void DebugMouseClick ( object sender , MouseButtonEventArgs e ) {
			switch ( click ) {
				case 0:
					LDebugMouseClick.Content = "1) " + e.GetPosition ( window );
					click++;
					break;
				case 1:
					LDebugMouseClick.Content += " || 2) " + e.GetPosition ( window );
					click = 0;
					;
					break;
			}
		}
		#endregion DEBUGGING
	}
}