﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;

namespace ComPortReader {
	// A custom event handler to alert the program a data was recieved
	public delegate void DataReceivedEventhandler ( object sender , DataPackage e );

	/// <summary>
	/// This class will attempt to connect to a serial port and read data from the port
	/// </summary>
	public class ComReader :IDisposable {
		private SerialPort port;
		public bool? isOpen => port?.IsOpen; 
		public event DataReceivedEventhandler dataReceived;

		/// <summary>
		/// Get all the available ports
		/// </summary>
		/// <returns>A list of string containing all open ports</returns>
		public static List<string> GetListofSerial ( ) => SerialPort.GetPortNames ( ).ToList ( );

		/// <summary>
		/// Create a serial port using the name of the port as reference
		/// </summary>
		/// <param name="portName">The port to connect to</param>
		public ComReader ( string portName ) {
			// Ensure that the name is legal
			if ( !string.IsNullOrWhiteSpace ( portName ) && portName.ToLower ( ).StartsWith ( "com" ) ) {
				port = new SerialPort ( portName , 9600 );
				port.Open ( );
				port.DiscardInBuffer();
				port.DataReceived += new SerialDataReceivedEventHandler ( DataReceivedEvent );
				return;
			}
			throw new Exception ( "Com Port not found" );
		}

		/// <summary>
		/// Try to automatically find the Arduino
		/// </summary>
		public ComReader ( SerialErrorReceivedEventHandler errorReceived) {
			port = new SerialPort ( ) { BaudRate = 9600 };

			port.DataReceived += new SerialDataReceivedEventHandler(DataReceivedEvent);
			port.ErrorReceived += errorReceived;
		}

		/// <summary>
		/// Tr to find the port the Arduino is in
		/// </summary>
		/// <returns></returns>
		public bool FindArduino ( ) {
			List<string> comList = GetListofSerial ( );
			port.Close ( );

			// go through each open com
			foreach ( string s in comList ) {
				// connect to the com port
				port.PortName = s;
				port.Open();
				port.DiscardInBuffer();
				System.Threading.Thread.Sleep(1000);

				try {
					byte[] data = new byte[5];
					port.Read(data, 0, 5);

					if (data[0] == 64 && data[1] == 114 && data[2] == 100 
							&& data[3] == 13 && data[4] == 10) {
						port.WriteLine("C#");
						return true;
					}
				} catch (Exception) { }

				// else we just close and try the next port
				port.Close ( );
			}
			return false;
		}

		/// <summary>
		/// Send string data to the serial port
		/// </summary>
		/// <param name="s">The string to send</param>
		public void SendData ( string s ) => port.Write ( Encoding.Default.GetBytes ( s ) , 0 , 0 );

		/// <summary>
		/// Send a byte array to the serial port
		/// </summary>
		/// <param name="bytearray"></param>
		public void SendData ( byte [ ] bytearray ) => port.Write ( bytearray , 0 , 0 );

		/// <summary>
		/// Called when the serial ported received a package. Attempt to read all the data
		/// and alert the main program
		private void DataReceivedEvent ( object sender , SerialDataReceivedEventArgs e ) {
			string data = port.ReadLine ( );
			//Console.WriteLine ( data );

			//port.DiscardInBuffer();

			// alert the main program that data is received
			byte [ ] dataArray = Encoding.Default.GetBytes ( data );
			dataReceived?.Invoke (this,  new DataPackage ( dataArray ) );
		}

		/// <summary>
		/// Free resources and remove port object
		/// </summary>
		/// <returns></returns>
		public void Dispose ( ) {
			Dispose ( true );
			GC.SuppressFinalize (this );
		}

		protected virtual void Dispose(bool dispose) {
			if ( dispose ) {
				port.DiscardInBuffer ( );
				port.DiscardOutBuffer ( );

				port.Close ( );
				port.Dispose ( );
				port = null;
			}
		}
	}

	/// <summary>
	/// A custom wrapper for EventArgs that contains the readed data from the serial port
	/// </summary>
	public class DataPackage : EventArgs {
		private byte[ ] data;
		// custom indexer
		public byte this [ int i ] => data [ i ];
		public int size => data.Length;
		/// <summary>
		/// Create an instance of DataPackage
		/// </summary>
		/// <param name="data">The array of binary received</param>
		public DataPackage ( byte [ ] data ) => this.data = data;
	}
}
